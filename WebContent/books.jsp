<%@page import="model.Books"%>
<%@page import="java.awt.print.Book"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form method="get" action="BookServlet">
  		<table>
   			<tr>
   				<th>Id</th><th>Title</th><th>Author</th><th>Pages</th><th>Category</th>
   			</tr>
   			   <tr><td>*{id}</td><td><%= session.getAttribute("title") %></td><td>*{author}</td><td>*{pages}</td><td>*{id_category}</td></tr>
   			<c:forEach items="${boosList}" var="book">
    		<tr><td>*{id}</td><td>*{title}</td><td>*{author}</td><td>*{pages}</td><td>*{id_category}</td></tr>
   			</c:forEach>
   
  		</table>
  	</form>

</body>
</html>