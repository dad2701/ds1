package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;

import model.Books;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet({ "/BookServlet", "/books", "/books/new" })
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Books book;
       
   
    public BookServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
         response.setContentType("text/html;charset=UTF-8");
         PrintWriter out = response.getWriter();
      
 			request.getRequestDispatcher("/books.jsp").include(request, response);	
 	
         try {
             out.println("<html>");
             out.println("<head>");
             out.println("<title>Problema de concurrencia</title>");
             out.println("</head>");
             out.println("<body>");
             out.println("El resultado es " + book.getTitle() + "");
             out.println("</body>");
             out.println("</html>");

         } finally {
           
         }
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	String way = request.getRequestURI();
		if(way.equals("/DS1/books/new")){
			request.getRequestDispatcher("/new_book.jsp").include(request, response);
			
			String title = request.getParameter("title");
			book = new Books(title, null,null,null,null);
			
		}else if(way.equals("/DS1/books")){
			request.getRequestDispatcher("/books.jsp").include(request, response);	
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
