package model;

public class Books {
	
	private String title;
	private String author;
	private Integer pages;
	private Integer id;
	private Integer id_category;
	
	public Books(String title, String author, Integer pages, Integer id, Integer id_category) {
		super();
		this.title = title;
		this.author = author;
		this.pages = pages;
		this.id = id;
		this.id_category = id_category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getPages() {
		return pages;
	}

	public void setPages(Integer pages) {
		this.pages = pages;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId_category() {
		return id_category;
	}

	public void setId_category(Integer id_category) {
		this.id_category = id_category;
	}
	
	

}
